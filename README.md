# Introduction

Belgrade is cool and Vampires are cool, so this is a collection of tabletop RPG stories for Vampire: Dark Ages, set in Belgrade.

## Weird Rules

The character sheets look a little weird because I have house rules.  I'll upload them at some point.

## Plot

The coterie are mass-embraced by a single sire of Clan Toreador.
She is killed for her crimes and the coterie are left to fend for themselves on the worst patch of peasant-land.

Over the coming years, the land becomes more and more Serbian.
The elder vampires, who mostly spoke Greek and Hungarian, lose touch with the common people, and the young coterie gain their edge: they are the only Cainites in the area who can really speak to most of the mortals.

# Why Belgrade?

Partly, because I live here.  Mostly because Belgrade's history's amazing.  It has:

* Romans (yes, in 1200 the Romans were still around and owned Serbia)
* Serbians deciding if they're really a country or not
* Hungarians gifting all of Belgrade to Serbia
* The final death of the Roman Empire
* The kingdom of Serbia wrecking everyone's shit (turns out they really were a country)

The history needs some serious revision, as I'd like to be reasonably historically accurate.
Except for the vampires.
