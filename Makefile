output: BbN.pdf
wwtex/backgrounds:  wwtex/Makefile
	cd wwtex && make backgrounds
wwtex/Makefile:
	git submodule update --init
BbN.pdf: $(wildcard *.tex) $(wildcard wwtex/*) wwtex/backgrounds
	xelatex --shell-escape main.tex
	makeglossaries main
	makeindex main.idx
	xelatex main.tex
	xelatex main.tex
	mv main.pdf BbN.pdf
all: Dark_Ages_Vampire.pdf Dark_Ages.pdf Vampire.pdf World_of_Darkness.pdf
clean:
	rm -fr *.aux *.toc *.acn *.log *.ptc *.out *.idx *.ist *.glo *.glg *.gls *.acr *.alg *.ilg *.ind *.pdf svg-inkscape
	cd vampire && rm -fr *.aux *.toc *.acn *.log *.ptc *.out *.idx *.ist *.glo *.glg *.gls *.acr *.alg *.ilg *.ind *.pdf
	cd wwtex && make clean
